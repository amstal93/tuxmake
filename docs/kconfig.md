# Kernel Configuration

Both the [command line](cli.md) and the [Python API](python.md) have two
arguments for configuration: `--kconfig`/`kconfig` and
`--kconfig-add`/`kconfig_add`.

**kconfig** can only be specified once, and specifies the *main kernel
configuration*. It can be specified in three different ways:

- a named configuration target (`defconfig`, etc);
- a path to a config file on the local filesystem;
- a (relative) path to a config file in the source tree;
- a URL to a config file, in which case TuxMake will download it.

**kconfig_add** specifies extra configuration to apply on top of the main
configuration specified by *kconfig*. It can be specified in the following
ways:

- an in-tree configuration target (e.g. `kvm_guest.config`);
- an explicit make target (`make:targetname`); TuxMake will call `make
  targetname` during the configuration step (e.g. `make:kselftest-merge` will
  cause `make kselftest-merge` to be called).
- an explicit *interactive* make target (`imake:targetname`); like
  `make:targetname`, but runs interactively (i.e. you can interact with it with
  the keyboard). Use for interactive targets such as `menuconfig`.
- a path to a config file on the local filesystem;
- a URL to a config file, in which case TuxMake will download it;
- a config fragment matching one of these:
    - `CONFIG_*=[y|m|n]`
    - `# CONFIG_* is not set`

`kconfig_add` can be specified multiple times. Any in-tree configuration target
and explicit make targets will be built with `make`, and then all of the others
will be saved to a local file in the order they were passed. They will be then
merged on top of the existing configuration by calling
`scripts/kconfig/merge_config.sh` and `make olddefconfig`.
