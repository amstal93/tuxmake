# TuxMake Python API

The public tuxmake API is provided by the `tuxmake.build` module, and is
documented here.

## The `Build` class

::: tuxmake.build.Build
    :docstring:
    :members: run status passed failed

## The `BuildInfo` class

::: tuxmake.build.BuildInfo
    :docstring:
    :members: status duration failed passed skipped

## The `build` wrapper function

::: tuxmake.build.build
    :docstring:
